from django.urls import path
from . import views


urlpatterns = [
                path('', views.home, name = 'home'),
                path('model',views.pre_model, name ='model'),
                path('upload', views.upload_Document, name = 'upload'),
                path('data_display', views.data_display, name = 'display'),
                path('ml', views.ml, name = 'ml' ),
                path('ml1', views.ml1, name = 'ml1'),
                path('end', views.end, name = 'end'),
                ]