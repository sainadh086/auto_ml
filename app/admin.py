from django.contrib import admin
from .models import Document, Column_info, LabelName
# Register your models here.

admin.site.register(Document)
admin.site.register(Column_info)
admin.site.register(LabelName)