import re
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.externals import joblib
REPLACE_BY_SPACE_RE = re.compile('[/(){}\[\]\|@,;]')
BAD_SYMBOLS_RE = re.compile('[^0-9a-z #+_]')
STOPWORDS = set(stopwords.words('english'))

def text_prepare(text):
    text = text.lower()
    text = REPLACE_BY_SPACE_RE.sub(' ', text)
    text = BAD_SYMBOLS_RE.sub('', text)
    text = ' '.join([x for x in text.split() if x and x not in STOPWORDS])
    return text
def tfidf_train(data):
    tfidf_data = joblib.load('Model/tfidf_model.pkl')
    tfidf_vectorizer = TfidfVectorizer(min_df=5, max_df=0.9, ngram_range=(1, 2), vocabulary=tfidf_data.vocabulary_)
    data = [text_prepare(data)]
    text_train = tfidf_vectorizer.fit_transform(data)
    lr = joblib.load('Model/nlp_model.pkl')
    y_pred = lr.predict(text_train)
    print(y_pred)
    if(y_pred== 1):
        return "Positive"
    
    return "Negative"