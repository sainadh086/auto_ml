from django.shortcuts import render
from .models import LabelName, Document, Column_info
from django.http import HttpResponse,HttpResponseRedirect
from django.core.files.storage import FileSystemStorage
from .nlp_train import tfidf_train
# Create your views here.

#------------------------------------------------------------------------

def home(request):

    return render(request, 'home.html')

#------------------------------------------------------------------------



def pre_model(request):
    if(request.method == "POST"):
        print("In request method")
        z = True
        z1 = False
        z2 = False
        data = request.POST['text_data']
        type_clf = request.POST['test']
        print(type_clf)
        if(type_clf == 'Sentiment'):
            z1 = True
            text = tfidf_train(data)
        else:
            z2 = True
            text = "working"
            return render(request, 'model.html', {'z':z,'z1':z1,'z2':z2})
        return render(request, 'model.html', {'z':z,'z1':z1,'output':text,'z2':z2})
    print("Outside")
    z = False
    z1 = False
    z2 = False
    return render(request, 'model.html', {'z':z,'z1':z1, 'z2':z2})



#-------------------------------------------------------------------------
#uploading a file

def upload_Document(request):
    z = False
    LabelName.objects.all().delete()
    if request.method == 'POST' and request.FILES['myfile']:
        z = True
        myfile = request.FILES['myfile']
        obj = Document(document=myfile, name = myfile.name)
        obj.save()
        return render(request, 'simple_upload.html', {'z':z})
    Document.objects.all().delete()
    Column_info.objects.all().delete()
    return render(request, 'simple_upload.html',{'z':z})




#---------------------------------------------------------------------------------------------------------------------

from django.views.decorators.csrf import csrf_protect
import pandas as pd
import numpy as np
import os
import re
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.externals import joblib

# storing data into database
def data_display(request):
    z = False
    try:
        file_name = Document.objects.all()[:1].values_list('document')
        print(file_name[0][0])
        data = pd.read_csv(file_name[0][0])
    except:
        return HttpResponse("<h2><center> Please upload the file </center> </h2>")
    col = []
    for i in data.columns:
        if(data[i].dtype == 'object'):


            obj = Column_info(column_name = i, column_type = data[i].dtype,str_unq = data[i].unique())
        else:
            obj = Column_info(column_name = i, column_type = data[i].dtype)
        obj.save()
        col.append(i)
    return render(request,'data.html',{'col':col})
    

# Training the model
def ml(request):
    test = False
    t1 = 'k'
    
    file_name = Document.objects.values_list('document')[:1][0]
    data = pd.read_csv(file_name[0])
    col = data.columns.to_list()
    if(request.method == 'POST'):
        try:
            t1 = request.POST['test']
        except:
            print("No value entered")
    if(t1=='Y'):
        test = True
        Label = LabelName.objects.values_list('Label')[0]
        #col.remove(Label)
        return render(request, 'index.html', {'test2':test,'col':col})
    elif(t1 == 'N'):
        test = True
        LabelName.objects.all().delete()
        os.remove('Model/model.pkl')
        return render(request, 'index.html', {'test4':test})
    if(request.method == 'POST'):
        Preprocess = request.POST['Preprocess']
        Label = request.POST['Label Name']
        split = request.POST['split']
        z = True
        obj = LabelName(Preprocess = Preprocess,Label = Label, split = split)
        obj.save()
    if(z):
        le = LabelEncoder()
        sc = StandardScaler()
        print("Printing label after decoding {}".format(Label))
        if(data[Label].dtype == 'object'):
            data[Label] = le.fit_transform(data[Label])
        y = data[Label].values
        data = data.drop(Label, axis = 1)
        for i in data.columns:
            c1 = re.findall(r"unnamed",i.lower())
            c2 = re.findall(r"id",i.lower())
            if(len(c1)>0 or len(c2)>0):
                data = data.drop(i,axis = 1)
        if(Preprocess == 'Y'):
            dop = []
            for i in data.keys():
                s = []
                if(data[i].isna().sum()>0):
                    if(data[i].dtypes == 'int64' or data[i].dtypes == 'float64'):
                        data[i].fillna(data[i].mean(),inplace = True)
                    elif(data[i].dtypes == 'object'):
                        data[i].fillna(data[i].mode()[0],inplace = True)
                if(data[i].dtype == 'object'):
                    for j in data[i]:
                        s.append(len(j))
                else:
                    s.append(0)
                if(max(s) > 30):
                    dop.append(i)
            for z1 in dop:
                print("Dropping the column : ", z1)
                data = data.drop(z1,axis = 1)
            cols = Column_info.objects.filter(column_type = 'object').values_list('column_name')
            print('Cols is',cols)
            for i in cols:
                if(Label != i[0]):
                    data[i] = le.fit_transform(data[i[0]])
        #X = data.drop(Label,axis=1)
        X = data.values
        from sklearn.model_selection import train_test_split
        X_train,X_test,y_train,y_test = train_test_split(X,y,test_size = int(split))
        X_train = sc.fit_transform(X_train)
        X_test = sc.fit_transform(X_test)
        from sklearn.linear_model import LogisticRegression
        lr  = LogisticRegression()
        lr.fit(X_train,y_train)
        y_pred = lr.predict(X_test)
        from sklearn.metrics import accuracy_score as ac
        ac1 = ac(y_test,y_pred)
		#LabelName.objects.all().delete()
        joblib.dump(lr, "Model/model.pkl")
        return render(request,'data.html',{'z':z,'label':Label,'ac1':ac1})
    return HttpResponse("<h2><center> Something Wrong with Training </center> </h2>")

    #---------------------------------------------------------------------------------------------------------------

# Testing on the data
def ml1(request):
    file_name = Document.objects.values_list('document')[:1][0]
    data = pd.read_csv(file_name[0])
    col = []
    for i in data.columns:
        c1  = re.findall("unnamed",i.lower())
        if(len(c1) == 0):
            col.append(i)
    Label = LabelName.objects.values_list('Label')[0]
    print("Label in test is ",Label)
    #col.remove(Label)
    df = pd.DataFrame()
    if(request.method == 'POST'):
        for i in col:
            n = []
            a = request.POST[i]
            #a = a.encode('ascii')
            n.append(a)
            df[i] = np.asarray(n)
            test = True
        if(test):
            le = LabelEncoder()
            #sc = StandardScaler()
            dop = []
            for i in df.columns:
                s = []
                if(data[i].dtype == "int64" or data[i].dtype == "float64" or data[i].dtype == "int32" or data[i].dtype == "float32"):
                    df[i] = df[i].astype("float32")
                if(data[i].dtype == 'object'):
                    for j in data[i]:
                        s.append(len(j))
                else:
                    s.append(0)
                c1 = re.findall(r"\id",i.lower())
                if(len(c1)>0):
                    dop.append(i)
                if(max(s)>0):
                    dop.append(s)
            if(len(dop)>0):
                for i in dop:
                    df = df.drop(i,axis = 1)
            for i in df.columns:
                if(data[i].dtype == "object"):
                    df[i] = le.fit_transform(df[i])
            lr = joblib.load("Model/model.pkl")
            x = df.values
            print(x)
            x = sc.fit_transform(x)
            y_pred = lr.predict(x)
            os.remove('Model/model.pkl')
            LabelName.objects.all().delete()
            return render(request,'index.html' , {'test3':test,'y_pred':y_pred[0]})
    else:
        return HttpResponse("<h3><center> Your test data is not aligned properly </center></h3>")
    return render(request, "index.html", {'test1':True})




def end(request):
    return HttpResponse("<h4><center> Thank you for using my services </center> </h4>")