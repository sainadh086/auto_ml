from django.db import models

# Create your models here.

class LabelName(models.Model):
	Label = models.CharField(max_length=50)
	Date = models.CharField(max_length=25, default='Blank')
	Preprocess = models.CharField(max_length = 1)
	split = models.IntegerField()
	test = models.CharField(max_length = 1)
	
	def __str__(self):
		return self.Label


class Document(models.Model):
	name = models.CharField(max_length=50)
	description = models.CharField(max_length=255, blank=True)
	document = models.FileField(upload_to="Documents/")

	def __str__(self):
		return self.name


class Column_info(models.Model):
	column_name = models.CharField(max_length=100)
	column_type = models.CharField(max_length=25)
	str_unq = models.CharField(max_length = 30, default='Blank')

	def __str__(self):
		return self.column_name

	

